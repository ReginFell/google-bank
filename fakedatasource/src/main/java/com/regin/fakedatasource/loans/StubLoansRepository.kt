package com.regin.fakedatasource.loans

import com.regin.googlebank.core.data.LoansRepository
import com.regin.googlebank.core.data.models.LoanDurationResource
import com.regin.googlebank.core.data.models.LoanResource
import com.regin.googlebank.core.data.models.LoanType
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class StubLoansRepository @Inject constructor() : LoansRepository {

    private val fakeLoans = listOf(
        LoanResource(
            "1234",
            "Consumer Loan",
            "Manage your financial life",
            "Up to 10 millions",
            LoanType.REFINANCE,
            LoanDurationResource.Small(12),
            LoanDurationResource.Regular(24),
            LoanDurationResource.Large(36)
        ),
        LoanResource(
            "1235",
            "Refinance",
            "Save your money by reducing overpayment",
            "Up to 10 millions",
            LoanType.CONSUMER,
            LoanDurationResource.Small(12),
            LoanDurationResource.Regular(24),
            LoanDurationResource.Large(36)
        ),
        LoanResource(
            "12357",
            "Auto Loan",
            "Special offers from the BMW and Volkswagen",
            "Interest rate: 12.7% - 15.9%",
            LoanType.AUTO,
            LoanDurationResource.Small(12),
            LoanDurationResource.Regular(24),
            LoanDurationResource.Large(36)
        ),
        LoanResource(
            "12359",
            "Refinance",
            "Manage your financial life",
            "Up to 10 millions",
            LoanType.MORTGAGE,
            LoanDurationResource.Small(12),
            LoanDurationResource.Regular(24),
            LoanDurationResource.Large(36)
        )
    )

    override fun observeLoans(): Observable<List<LoanResource>> = Observable.fromCallable { fakeLoans }
        .subscribeOn(Schedulers.io())

    override fun observeLoan(guid: String): Observable<LoanResource> = Observable.fromCallable {
        fakeLoans.first { it.guid == guid }
    }

}