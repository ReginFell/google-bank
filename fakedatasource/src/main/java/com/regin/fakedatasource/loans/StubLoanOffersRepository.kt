package com.regin.fakedatasource.loans

import com.regin.googlebank.core.data.LoanOffersRepository
import com.regin.googlebank.core.data.models.OfferResource
import io.reactivex.Observable
import java.util.concurrent.TimeUnit
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class StubLoanOffersRepository @Inject constructor() : LoanOffersRepository {

    private val fakeStandardOffers = listOf(
            OfferResource(
                    "12341243124",
                    "https://d1nhio0ox7pgb.cloudfront.net/_img/g_collection_png/standard/256x256/bank_building.png",
                    "Citibank",
                    1330f,
                    15.5f,
                    15.9f
            ),
            OfferResource(
                    "555",
                    "https://d2v4mp3ljzx6qv.cloudfront.net/sites/default/files/LogoCOLOR_PNC_bank.png",
                    "PNC Financial Service",
                    1550f,
                    16.7f,
                    18.9f
            )
    )

    private val fakeSpecialOffers = listOf(
            OfferResource(
                    "12345",
                    "https://d1nhio0ox7pgb.cloudfront.net/_img/g_collection_png/standard/256x256/bank_building.png",
                    "Citibank",
                    1500f,
                    15.5f,
                    interestRateOld = 20.3f
            ),
            OfferResource(
                    "123456",
                    "https://d1nhio0ox7pgb.cloudfront.net/_img/g_collection_png/standard/256x256/currency_pound.png",
                    "PNC Financial Service",
                    1200f,
                    12.5f,
                    interestRateOld = 18.3f
            )
    )

    override fun observeStandardOffers(): Observable<List<OfferResource>> = Observable.fromCallable { fakeStandardOffers }

    override fun observeSpecialOffers(): Observable<List<OfferResource>> = Observable.fromCallable { fakeSpecialOffers }
            .delay(2, TimeUnit.SECONDS)
}