package com.regin.fakedatasource.loans

import android.graphics.Color
import com.regin.googlebank.core.data.ChatRepository
import com.regin.googlebank.core.data.models.Card
import com.regin.googlebank.core.data.models.CardType
import com.regin.googlebank.core.data.models.Message
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.subjects.BehaviorSubject
import java.util.*
import java.util.concurrent.TimeUnit
import java.util.concurrent.atomic.AtomicInteger
import javax.inject.Inject

class StubChatRepository @Inject constructor() : ChatRepository {

    private var atomicCounter: AtomicInteger = AtomicInteger(0)

    private val cards = listOf(
            Card(
                    "1234",
                    "For money",
                    Color.parseColor("#de1031"),
                    20000f,
                    "https://images.pexels.com/photos/326875/pexels-photo-326875.jpeg?auto=compress&cs=tinysrgb&h=750&w=1260",
                    "**** **** **** *133",
                    "12/2020",
                    CardType.VISA
            ),
            Card(
                    "12345",
                    "Salary",
                    Color.parseColor("#de1031"),
                    25000f,
                    "https://images.pexels.com/photos/326875/pexels-photo-326875.jpeg?auto=compress&cs=tinysrgb&h=750&w=1260",
                    "**** **** **** *333",
                    "12/2020",
                    CardType.VISA
            ),
            Card(
                    "12345",
                    "Another",
                    Color.parseColor("#de1031"),
                    30000f,
                    "https://images.pexels.com/photos/326875/pexels-photo-326875.jpeg?auto=compress&cs=tinysrgb&h=750&w=1260",
                    "**** **** **** *233",
                    "12/2020",
                    CardType.VISA
            )
    )

    private val botPhrases = listOf(
            Message.TextMessage(
                    UUID.randomUUID().toString(),
                    BOT_AVATAR_PHOTO,
                    false,
                    "Send one more message to see your cards"
            ),
            Message.CardChooserMessage(
                    UUID.randomUUID().toString(),
                    BOT_AVATAR_PHOTO,
                    false,
                    cards
            )
    )

    private val behaviorSubject: BehaviorSubject<MutableList<Message>> = BehaviorSubject.createDefault(
            mutableListOf(
                    Message.TextMessage(UUID.randomUUID().toString(), BOT_AVATAR_PHOTO, false, "Hello, may I help you?") as Message
            )
    )

    override fun observeChat(offerGuid: String): Observable<List<Message>> {
        return behaviorSubject
                .flatMap { Observable.just(it) }
    }

    @Synchronized
    override fun sendMessage(text: String): Completable {
        return Single.fromCallable {
            val messages = behaviorSubject.value!!
            messages.add(Message.TextMessage(UUID.randomUUID().toString(), USER_AVATAR_PHOTO, true, text))
            if (atomicCounter.get() < botPhrases.size) {
                messages.add(botPhrases[atomicCounter.getAndIncrement()])
            }

            messages
        }
                .delay(500, TimeUnit.MILLISECONDS)
                .flatMapCompletable {
                    Completable.fromAction { behaviorSubject.onNext(it) }
                }
    }

    companion object {
        private const val USER_AVATAR_PHOTO =
                "https://images.pexels.com/photos/326875/pexels-photo-326875.jpeg?auto=compress&cs=tinysrgb&h=750&w=1260"
        private const val BOT_AVATAR_PHOTO =
                "https://images.pexels.com/photos/7517/animal-sitting-animals-inside.jpg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260"
    }
}