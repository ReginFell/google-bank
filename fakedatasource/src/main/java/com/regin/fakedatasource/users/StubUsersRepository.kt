package com.regin.fakedatasource.users

import com.regin.googlebank.core.data.UsersRepository
import com.regin.googlebank.core.data.models.UserResource
import dagger.Reusable
import io.reactivex.Observable
import javax.inject.Inject

@Reusable
class StubUsersRepository @Inject constructor() : UsersRepository {

    private val me: UserResource = UserResource("userId", 56)

    override fun me(): Observable<UserResource> = Observable.just(me)

}