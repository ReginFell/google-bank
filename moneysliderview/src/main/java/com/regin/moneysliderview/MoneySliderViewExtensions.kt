package com.regin.moneysliderview

import io.reactivex.subjects.PublishSubject

fun MoneySliderView.changes(): PublishSubject<Float> {
    val publishSubject: PublishSubject<Float> = PublishSubject.create()

    setValueChangesListener { publishSubject.onNext(it) }

    return publishSubject
}