package com.regin.moneysliderview

import android.animation.ArgbEvaluator
import android.animation.ValueAnimator
import android.annotation.SuppressLint
import android.content.Context
import android.content.res.ColorStateList
import android.content.res.TypedArray
import android.graphics.Rect
import android.graphics.drawable.Drawable
import android.os.Parcel
import android.os.Parcelable
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.TouchDelegate
import android.view.View
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.core.widget.ImageViewCompat
import androidx.customview.widget.ViewDragHelper
import kotlinx.android.synthetic.main.view_money_slider.view.*
import java.text.DecimalFormat
import java.text.NumberFormat
import kotlin.math.absoluteValue

typealias OnValueChangesListener = (value: Float) -> Unit

class MoneySliderView @JvmOverloads constructor(
        context: Context,
        attrs: AttributeSet? = null,
        private val defStyleAttrs: Int = 0
) : ConstraintLayout(context, attrs, defStyleAttrs) {

    private val touchableArea = Rect()

    private var initialBackgroundColor: Int = 0
    private var initialFabButtonColor: Int = 0
    private var initialFabIconColor: Int = 0
    private var initialTextContainerBackground: Drawable? = null
    private var initialTextColor: Int = 0
    private var initialLabelTextColor: Int = 0

    private var selectedBackgroundColor: Int = 0
    private var selectedFabButtonColor: Int = 0
    private var selectedFabIconColor: Int = 0
    private var selectedTextContainerBackground: Drawable? = null
    private var selectedTextColor: Int = 0
    private var selectedLabelTextColor: Int = 0

    private var fabIcon: Drawable? = null

    private var minimumValue: Float = 0f
    private var maximumValue: Float = 0f
    private var defaultValue: Float = 0f

    private var initialLabel: String? = null

    private val viewDragHelper: ViewDragHelper

    private var onValueChangesListener: OnValueChangesListener? = null

    init {
        inflate(context, R.layout.view_money_slider, this)

        viewDragHelper = ViewDragHelper.create(this, 1f, object : ViewDragHelper.Callback() {

            private val initialButtonTop by lazy { fab.top }

            override fun tryCaptureView(child: View, pointerId: Int): Boolean = child == fab

            override fun clampViewPositionHorizontal(child: View, left: Int, dx: Int): Int {
                val rightPadding = valueField.paddingLeft
                val leftPadding = valueField.paddingLeft
                val range = getViewHorizontalDragRange(child)

                return Math.min(Math.max(left, rightPadding + leftPadding), range)
            }

            override fun clampViewPositionVertical(child: View, top: Int, dy: Int): Int = initialButtonTop
            override fun getViewVerticalDragRange(child: View): Int = initialButtonTop

            override fun getViewHorizontalDragRange(child: View): Int = valueField.measuredWidth - fab.measuredWidth

            override fun onViewCaptured(capturedChild: View, activePointerId: Int) {
                super.onViewCaptured(capturedChild, activePointerId)
                parent.requestDisallowInterceptTouchEvent(true)
            }

            override fun onViewReleased(releasedChild: View, xvel: Float, yvel: Float) {
                super.onViewReleased(releasedChild, xvel, yvel)
                parent.requestDisallowInterceptTouchEvent(false)
            }

            override fun onViewPositionChanged(changedView: View, left: Int, top: Int, dx: Int, dy: Int) {
                val leftEdge = valueField.paddingLeft + valueField.paddingLeft
                val rightEdge = getViewHorizontalDragRange(changedView)

                val percent = getPercent(left.toFloat(), rightEdge.toFloat(), leftEdge.toFloat())

                val newValue = Math.max(maximumValue * (1 - percent), minimumValue)

                onValueChangesListener?.invoke(newValue)

                valueField.text = formatAsMoney(newValue.absoluteValue)

                if (!isSelected) {
                    isSelected = true
                }
            }
        })

        attrs?.let { inflateAttributes(it) }
    }

    private fun inflateAttributes(attrs: AttributeSet) {
        val attributes = context.obtainStyledAttributes(attrs, R.styleable.MoneySliderView, defStyleAttrs, 0)

        initialBackgroundColor =
                extractColor(attributes, R.styleable.MoneySliderView_initialBackground, R.color.colorPrimary)
        initialFabButtonColor =
                extractColor(attributes, R.styleable.MoneySliderView_initialFabButtonColor, R.color.colorAccent)
        initialFabIconColor =
                extractColor(attributes, R.styleable.MoneySliderView_initialFabIconColor, R.color.colorPrimary)

        initialTextContainerBackground = ContextCompat.getDrawable(
                context,
                attributes.getResourceId(
                        R.styleable.MoneySliderView_initialTextContainerBackground,
                        R.drawable.shape_grey_background
                )
        )

        initialTextColor =
                extractColor(attributes, R.styleable.MoneySliderView_initialTextColor, android.R.color.black)

        initialLabelTextColor =
                extractColor(
                        attributes,
                        R.styleable.MoneySliderView_initialLabelTextColor,
                        R.color.textColorSecondary
                )

        selectedBackgroundColor =
                extractColor(attributes, R.styleable.MoneySliderView_selectedBackground, R.color.colorAccent)
        selectedFabButtonColor =
                extractColor(attributes, R.styleable.MoneySliderView_selectedFabButtonColor, R.color.colorPrimary)
        selectedFabIconColor =
                extractColor(attributes, R.styleable.MoneySliderView_selectedFabIconColor, R.color.colorAccent)

        selectedTextContainerBackground = ContextCompat.getDrawable(
                context,
                attributes.getResourceId(
                        R.styleable.MoneySliderView_selectedTextContainerBackground,
                        R.drawable.shape_border
                )
        )

        selectedTextColor =
                extractColor(attributes, R.styleable.MoneySliderView_selectedTextColor, android.R.color.white)

        selectedLabelTextColor =
                extractColor(attributes, R.styleable.MoneySliderView_selectedLabelTextColor, R.color.colorPrimary)

        fabIcon = ContextCompat.getDrawable(
                context,
                attributes.getResourceId(
                        R.styleable.MoneySliderView_fabIcon,
                        R.drawable.ic_expand_collapse
                )
        )

        minimumValue = attributes.getInt(R.styleable.MoneySliderView_minimumValue, 0).toFloat()
        maximumValue = attributes.getInt(R.styleable.MoneySliderView_maximumValue, 0).toFloat()
        defaultValue = attributes.getInt(R.styleable.MoneySliderView_defaultValue, 0).toFloat()

        initialLabel = attributes.getString(R.styleable.MoneySliderView_label)

        attributes.recycle()

        isSelected = false
        valueField.text = formatAsMoney(defaultValue)
        fab.setImageDrawable(fabIcon)
        label.text = initialLabel
    }

    override fun onLayout(changed: Boolean, left: Int, top: Int, right: Int, bottom: Int) {
        super.onLayout(changed, left, top, right, bottom)
        if (changed) {
            fab.getHitRect(touchableArea)
            touchableArea.top += EXTRA_TOUCHABLE_AREA
            touchableArea.bottom += EXTRA_TOUCHABLE_AREA
            touchableArea.left += EXTRA_TOUCHABLE_AREA
            touchableArea.right += EXTRA_TOUCHABLE_AREA
            @SuppressLint("DrawAllocation")
            touchDelegate = TouchDelegate(touchableArea, fab)
        }
    }

    override fun onInterceptTouchEvent(ev: MotionEvent): Boolean {
        return viewDragHelper.shouldInterceptTouchEvent(ev)
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onTouchEvent(event: MotionEvent): Boolean {
        viewDragHelper.processTouchEvent(event)
        return true
    }

    override fun setSelected(isSelected: Boolean) {
        super.setSelected(isSelected)

        if (isSelected) {
            valueField.background = selectedTextContainerBackground
            transformColor(initialBackgroundColor, selectedBackgroundColor) { setBackgroundColor(it) }
            transformColor(initialFabButtonColor, selectedFabButtonColor) {
                fab.backgroundTintList = ColorStateList.valueOf(it)
            }
            transformColor(initialTextColor, selectedTextColor) { valueField.setTextColor(it) }
            transformColor(initialLabelTextColor, selectedLabelTextColor) { colorInt ->
                label.setTextColor(colorInt)
                valueField.compoundDrawables.filter { it != null }.forEach { it.setTint(colorInt) }
            }

            transformColor(initialFabIconColor, selectedFabIconColor) {
                ImageViewCompat.setImageTintList(fab, ColorStateList.valueOf(it))
            }
        } else {
            valueField.background = initialTextContainerBackground
            transformColor(selectedBackgroundColor, initialBackgroundColor) { setBackgroundColor(it) }
            transformColor(selectedFabButtonColor, initialFabButtonColor) {
                fab.supportBackgroundTintList = ColorStateList.valueOf(it)
            }
            transformColor(selectedTextColor, initialTextColor) { valueField.setTextColor(it) }
            transformColor(selectedFabIconColor, initialFabIconColor) {
                ImageViewCompat.setImageTintList(fab, ColorStateList.valueOf(it))
            }

            transformColor(selectedLabelTextColor, initialLabelTextColor) { colorInt ->
                label.setTextColor(colorInt)
                valueField.compoundDrawables.filter { it != null }.forEach { it.setTint(colorInt) }
            }
        }
    }

    override fun onSaveInstanceState(): Parcelable? {
        val superState = super.onSaveInstanceState()
        val savedState = SaveState(superState)
        savedState.isSelected = isSelected
        return savedState
    }

    override fun onRestoreInstanceState(state: Parcelable?) {
        if (state !is SaveState) {
            super.onRestoreInstanceState(state)
            return
        }

        super.onRestoreInstanceState(state.superState)
        isSelected = state.isSelected
    }

    fun setValue(value: Float) {
        valueField.text = formatAsMoney(value)
    }

    fun setValueChangesListener(onValueChangesListener: OnValueChangesListener) {
        this.onValueChangesListener = onValueChangesListener
    }

    fun removeValueChangesListener() {
        this.onValueChangesListener = null
    }

    fun setValues(minimumValue: Float, maximumValue: Float, defaultValue: Float) {
        this.minimumValue = minimumValue
        this.maximumValue = maximumValue
        this.defaultValue = defaultValue
    }

    private fun getPercent(current: Float, minimumValue: Float, maximumValue: Float): Float {
        return (current - minimumValue) / (maximumValue - minimumValue)
    }

    private fun transformColor(startColor: Int, endColor: Int, apply: (value: Int) -> Unit) {
        ValueAnimator.ofInt(startColor, endColor)
                .apply {
                    setEvaluator(ArgbEvaluator())
                    addUpdateListener { animation ->
                        apply(animation.animatedValue as Int)
                    }
                    start()
                }
    }

    private fun extractColor(attributes: TypedArray, resId: Int, defaultResId: Int): Int {
        return ContextCompat.getColor(
                context,
                attributes.getResourceId(resId, defaultResId)
        )
    }

    private fun formatAsMoney(value: Float): String {
        val formatter = NumberFormat.getCurrencyInstance()
        val decimalFormatSymbols = (formatter as DecimalFormat).decimalFormatSymbols
        decimalFormatSymbols.currencySymbol = ""
        formatter.decimalFormatSymbols = decimalFormatSymbols
        return formatter.format(value)
    }

    private class SaveState : BaseSavedState {
        var isSelected: Boolean = false

        private constructor(parcel: Parcel) : super(parcel) {
            isSelected = parcel.readByte().toInt() != 0
        }

        constructor(superState: Parcelable) : super(superState)

        override fun writeToParcel(parcel: Parcel, flags: Int) {
            super.writeToParcel(parcel, flags)
            parcel.writeInt(if (isSelected) 1 else 0)
        }

        override fun describeContents(): Int {
            return 0
        }

        companion object CREATOR : Parcelable.Creator<SaveState> {
            override fun createFromParcel(parcel: Parcel): SaveState {
                return SaveState(parcel)
            }

            override fun newArray(size: Int): Array<SaveState?> {
                return arrayOfNulls(size)
            }
        }
    }

    companion object {
        private const val EXTRA_TOUCHABLE_AREA = 100
    }
}
