package com.regin.googlebank.inject.datasource

import com.regin.fakedatasource.loans.StubChatRepository
import com.regin.fakedatasource.loans.StubLoanOffersRepository
import com.regin.fakedatasource.loans.StubLoansRepository
import com.regin.fakedatasource.users.StubUsersRepository
import com.regin.googlebank.core.data.ChatRepository
import com.regin.googlebank.core.data.LoanOffersRepository
import com.regin.googlebank.core.data.LoansRepository
import com.regin.googlebank.core.data.UsersRepository
import dagger.Binds
import dagger.Module

@Module
abstract class DataSourceBindingModule {

    @Binds
    abstract fun bindLoansRepository(impl: StubLoansRepository): LoansRepository

    @Binds
    abstract fun bindLoanOffersRepository(impl: StubLoanOffersRepository): LoanOffersRepository

    @Binds
    abstract fun bindUsersRepository(impl: StubUsersRepository): UsersRepository

    @Binds
    abstract fun bindChatRepository(impl: StubChatRepository): ChatRepository

}