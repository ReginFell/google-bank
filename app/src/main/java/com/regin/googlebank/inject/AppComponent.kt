package com.regin.googlebank.inject

import com.regin.googlebank.Application
import com.regin.googlebank.navigation.NavigationBindingModule
import com.regin.googlebank.inject.datasource.DataSourceBindingModule
import dagger.Component
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AppModule::class,
        DataSourceBindingModule::class,
        AndroidSupportInjectionModule::class,
        NavigationBindingModule::class
    ]
)
interface AppComponent {

    fun inject(application: Application)

}