package com.regin.googlebank.inject

import com.regin.googlebank.core.ui.ApplicationRouter
import com.regin.googlebank.navigation.AppRouter
import dagger.Module
import dagger.Provides
import ru.terrakok.cicerone.Cicerone
import ru.terrakok.cicerone.NavigatorHolder

@Module
class AppModule {

    private val cicerone: Cicerone<AppRouter> = Cicerone.create(AppRouter())

    @Provides
    fun provideNavigationHolder(): NavigatorHolder {
        return cicerone.navigatorHolder
    }

    @Provides
    fun provideRouter(): ApplicationRouter {
        return cicerone.router
    }
}