package com.regin.googlebank

import android.app.Activity
import android.app.Application
import androidx.fragment.app.Fragment
import com.regin.googlebank.inject.AppComponent
import com.regin.googlebank.inject.DaggerAppComponent
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasActivityInjector
import dagger.android.support.HasSupportFragmentInjector
import javax.inject.Inject

class Application : Application(), HasActivityInjector, HasSupportFragmentInjector {

    private val appComponent: AppComponent by lazy { DaggerAppComponent.builder().build() }

    @Inject lateinit var activityInjector: DispatchingAndroidInjector<Activity>
    @Inject lateinit var fragmentInjector: DispatchingAndroidInjector<Fragment>

    override fun supportFragmentInjector(): AndroidInjector<Fragment> = fragmentInjector
    override fun activityInjector(): AndroidInjector<Activity> = activityInjector

    override fun onCreate() {
        super.onCreate()
        appComponent.inject(this)
    }

}