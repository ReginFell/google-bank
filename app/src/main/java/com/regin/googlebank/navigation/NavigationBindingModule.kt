package com.regin.googlebank.navigation

import com.regin.loans.details.LoanDetailsBindingModule
import com.regin.loans.list.LoansBindingModule
import com.regin.offers.details.OfferDetailsBindingModule
import com.regin.offers.list.OffersBindingModule
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class NavigationBindingModule {

    @ContributesAndroidInjector(
        modules = [
            NavigationModule::class,
            LoansBindingModule::class,
            LoanDetailsBindingModule::class,
            OffersBindingModule::class,
            OfferDetailsBindingModule::class
        ]
    )
    abstract fun bindNavigationActivity(): NavigationActivity

}
