package com.regin.googlebank.navigation

import android.view.View
import com.regin.googlebank.core.ui.ApplicationRouter
import com.regin.googlebank.core.ui.models.loan.LoanFirstStepBuilder
import com.regin.googlebank.core.ui.models.loan.LoanSecondStepBuilder
import com.regin.googlebank.core.ui.utils.navigation.command.ForwardWithTransition
import com.regin.googlebank.core.ui.utils.navigation.screen.TransitionScreen
import com.regin.googlebank.navigation.screen.LoanDetailsScreen
import com.regin.googlebank.navigation.screen.OfferDetailsScreen
import com.regin.googlebank.navigation.screen.OffersScreen
import com.regin.loans.list.LoansScreen
import ru.terrakok.cicerone.Router
import ru.terrakok.cicerone.commands.BackTo
import ru.terrakok.cicerone.commands.Forward

class AppRouter : Router(), ApplicationRouter {

    override fun backToRoot() {
        executeCommands(BackTo(LoansScreen()))
    }

    override fun navigateToOffersScreen(loanFirstStepBuilder: LoanFirstStepBuilder) {
        executeCommands(Forward(OffersScreen(loanFirstStepBuilder)))
    }

    override fun navigateToOfferDetailsScreen(loanSecondStepBuilder: LoanSecondStepBuilder,
                                              sharedView: View,
                                              transitionName: String) {
        executeCommands(ForwardWithTransition(OfferDetailsScreen(loanSecondStepBuilder, sharedView, transitionName)))
    }

    override fun navigateToLoanDetails(loanGuid: String, sharedView: View, transitionName: String) {
        navigateToWithTransition(
                LoanDetailsScreen(
                        loanGuid,
                        sharedView,
                        transitionName
                )
        )
    }

    private fun navigateToWithTransition(screen: TransitionScreen) {
        executeCommands(ForwardWithTransition(screen))
    }

}