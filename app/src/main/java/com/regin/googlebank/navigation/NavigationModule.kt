package com.regin.googlebank.navigation

import com.regin.googlebank.R
import com.regin.googlebank.core.ui.utils.navigation.ApplicationNavigator
import dagger.Module
import dagger.Provides
import ru.terrakok.cicerone.Navigator

@Module
class NavigationModule {

    @Provides
    fun provideNavigator(activity: NavigationActivity): Navigator {
        return ApplicationNavigator(activity, containerId = R.id.navContainer)
    }
}