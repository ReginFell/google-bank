package com.regin.googlebank.navigation.screen

import com.regin.googlebank.core.ui.utils.navigation.SupportAppScreen
import com.regin.googlebank.core.ui.models.loan.LoanFirstStepBuilder
import com.regin.offers.list.OffersFragment

class OffersScreen(loanFirstStepBuilder: LoanFirstStepBuilder) : SupportAppScreen() {

    override val fragment = OffersFragment.newInstance(loanFirstStepBuilder)
}