package com.regin.googlebank.navigation.screen

import android.view.View
import com.regin.googlebank.core.ui.utils.navigation.screen.TransitionScreen
import com.regin.loans.R
import com.regin.loans.details.LoanDetailsFragment

class LoanDetailsScreen(
        loanGuid: String,
        sharedView: View,
        transitionName: String
) : TransitionScreen() {

    override val fragment = LoanDetailsFragment.newInstance(loanGuid, transitionName)

    override val sharedElement = sharedView

    override val sharedElementReturnTransaction: Int = R.transition.default_transition
    override val sharedElementEnterTransition: Int = R.transition.default_transition
}