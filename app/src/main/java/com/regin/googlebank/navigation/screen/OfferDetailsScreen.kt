package com.regin.googlebank.navigation.screen

import android.view.View
import com.regin.googlebank.R
import com.regin.googlebank.core.ui.models.loan.LoanFirstStepBuilder
import com.regin.googlebank.core.ui.models.loan.LoanSecondStepBuilder
import com.regin.googlebank.core.ui.utils.navigation.screen.TransitionScreen
import com.regin.offers.details.OfferDetailsFragment

class OfferDetailsScreen(
        loanSecondStepBuilder: LoanSecondStepBuilder,
        sharedView: View,
        transitionName: String
) : TransitionScreen() {

    override val fragment = OfferDetailsFragment.newInstance(loanSecondStepBuilder, transitionName)

    override val sharedElement = sharedView

    override val sharedElementReturnTransaction: Int = R.transition.default_transition
    override val sharedElementEnterTransition: Int = R.transition.default_transition
}