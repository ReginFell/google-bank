package com.regin.offers.views

import android.content.Context
import android.util.AttributeSet
import android.widget.LinearLayout
import androidx.core.content.ContextCompat
import com.regin.googlebank.core.ui.extensions.formatAsMoney
import com.regin.googlebank.core.ui.utils.color
import com.regin.googlebank.core.ui.utils.plus
import com.regin.googlebank.core.ui.utils.spannable
import com.regin.offers.R
import kotlinx.android.synthetic.main.view_selected_loan.view.duration as durationView
import kotlinx.android.synthetic.main.view_selected_loan.view.loanTitle as loanTitleView
import kotlinx.android.synthetic.main.view_selected_loan.view.money as moneyView

class SelectedLoanView @JvmOverloads constructor(
        context: Context,
        attrs: AttributeSet? = null,
        defStyleAttrs: Int = 0
) : LinearLayout(context, attrs, defStyleAttrs) {

    private val colorBlack = ContextCompat.getColor(context, R.color.textColorBlack)
    private val colorSecondary = ContextCompat.getColor(context, R.color.textColorSecondary)

    init {
        inflate(context, R.layout.view_selected_loan, this)
    }

    fun setLoan(loanTitle: String, moneyAmount: Float, duration: Int) {
        loanTitleView.text = loanTitle
        moneyView.text = spannable { color(colorSecondary, MONEY_CHAR.toString() + " ") + color(colorBlack, moneyAmount.formatAsMoney(false)) }
        durationView.text = spannable {
            color(colorBlack, duration.toString()) + " " +
                    color(colorSecondary, context.getString(R.string.months))
        }
    }

    companion object {
        private const val MONEY_CHAR = '$'
    }
}