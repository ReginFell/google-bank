package com.regin.offers.list

import androidx.lifecycle.ViewModel
import com.regin.googlebank.core.inject.ViewModelKey
import dagger.Binds
import dagger.Module
import dagger.android.ContributesAndroidInjector
import dagger.multibindings.IntoMap

@Module
abstract class OffersBindingModule {

    @ContributesAndroidInjector(modules = [OffersModule::class])
    abstract fun bindLoanOffersFragment(): OffersFragment

    @Binds
    @IntoMap
    @ViewModelKey(OffersViewModel::class)
    abstract fun bindLoanOffersViewModel(viewModel: OffersViewModel): ViewModel

}