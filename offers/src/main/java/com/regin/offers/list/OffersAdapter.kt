package com.regin.offers.list

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.regin.googlebank.core.ui.extensions.formatAsMoney
import com.regin.googlebank.core.ui.models.SharedElementListItem
import com.regin.googlebank.core.ui.utils.bold
import com.regin.googlebank.core.ui.utils.plus
import com.regin.googlebank.core.ui.utils.spannable
import com.regin.googlebank.core.ui.utils.strike
import com.regin.offers.R
import com.regin.offers.list.model.OfferItem
import io.reactivex.subjects.PublishSubject
import kotlinx.android.synthetic.main.item_offer.view.*
import kotlinx.android.synthetic.main.item_offer_title.view.*
import kotlinx.android.synthetic.main.item_offer.view.interestRate as interestRateView
import kotlinx.android.synthetic.main.item_offer.view.monthlyPayment as monthlyPaymentView

class OffersAdapter : ListAdapter<OfferItem, RecyclerView.ViewHolder>(ItemCallback) {

    companion object {
        private const val TRANSITION_TAG = "transition"
    }

    val onItemClickedSubject = PublishSubject.create<SharedElementListItem<OfferItem.Offer>>()

    override fun getItemViewType(position: Int): Int {
        return getItem(position).viewType
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            OfferItem.ITEM_TYPE_CONTENT -> {
                val view = LayoutInflater.from(parent.context).inflate(R.layout.item_offer, parent, false)
                ViewHolder(view)
            }
            OfferItem.ITEM_TYPE_TITLE -> {
                TitleViewHolder(
                        LayoutInflater.from(parent.context)
                                .inflate(R.layout.item_offer_title, parent, false)
                )
            }
            OfferItem.ITEM_TYPE_PROGRESS -> {
                ProgressViewHolder(
                        LayoutInflater.from(parent.context)
                                .inflate(R.layout.item_offer_progress, parent, false)
                )
            }
            else -> throw RuntimeException("Unexpected holder type $viewType")
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        with(getItem(position)) {
            when (this) {
                is OfferItem.Offer -> {
                    (holder as ViewHolder).bind(this)
                }
                is OfferItem.Title -> {
                    (holder as TitleViewHolder).bind(this)
                }
            }
        }
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(offer: OfferItem.Offer) {
            with(itemView) {
                val transitionName = buildTransactionName(offer.offerGuid)
                setOnClickListener {
                    onItemClickedSubject.onNext(SharedElementListItem(offer, this, transitionName))
                }
                Glide.with(offerIcon)
                        .load(offer.offerIconUrl)
                        .apply(RequestOptions().circleCrop())
                        .into(offerIcon)

                title.text = offer.offerName
                monthlyPaymentView.text = offer.offerMonthlyPayment.formatAsMoney()
                interestRateView.text = buildInterest(offer.interestRateFrom, offer.interestRateTo, offer.interestRateOld)

                if (offer.isSpecial) {
                    monthlyPaymentView.setTextColor(ContextCompat.getColor(context, R.color.special_offers_color))
                    interestRateView.setTextColor(ContextCompat.getColor(context, R.color.special_offers_color))
                } else {
                    monthlyPaymentView.setTextColor(ContextCompat.getColor(context, R.color.textColorBlack))
                    interestRateView.setTextColor(ContextCompat.getColor(context, R.color.textColorBlack))
                }

                this.transitionName = transitionName
            }
        }

        private fun buildInterest(interestFrom: Float, interestTo: Float?, interestOld: Float?): CharSequence {
            return when {
                interestTo == null && interestOld == null -> interestFrom.toString()
                interestTo == null -> spannable { bold("$interestFrom ") + strike(interestOld.toString()) }
                else -> "$interestFrom - $interestTo"
            }
        }
    }

    inner class TitleViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(offerItem: OfferItem.Title) {
            itemView.offersTitle.text = itemView.context.getString(offerItem.title)
        }
    }

    private fun buildTransactionName(guid: String): String {
        return "$TRANSITION_TAG$guid"
    }

    inner class ProgressViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    private object ItemCallback : DiffUtil.ItemCallback<OfferItem>() {
        override fun areItemsTheSame(oldListItem: OfferItem, newListItem: OfferItem): Boolean {
            return if (oldListItem is OfferItem.Offer && newListItem is OfferItem.Offer) {
                oldListItem.offerGuid == newListItem.offerGuid
            } else {
                oldListItem == newListItem
            }
        }

        override fun areContentsTheSame(oldListItem: OfferItem, newListItem: OfferItem): Boolean {
            return oldListItem == newListItem
        }
    }
}
