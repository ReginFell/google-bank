package com.regin.offers.list

import com.regin.googlebank.core.data.LoanOffersRepository
import com.regin.googlebank.core.data.UsersRepository
import com.regin.googlebank.core.ui.ApplicationRouter
import com.regin.googlebank.core.ui.BaseViewModel
import com.regin.googlebank.core.ui.models.SharedElementListItem
import com.regin.googlebank.core.ui.models.loan.LoanFirstStepBuilder
import com.regin.googlebank.core.ui.models.loan.LoanSecondStepBuilder
import com.regin.googlebank.core.ui.models.user.model.User
import com.regin.googlebank.core.ui.models.user.model.UserMapper
import com.regin.offers.list.model.OfferItem
import com.regin.offers.list.model.OfferItemListCreator
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.Observables
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.PublishSubject
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class OffersViewModel @Inject constructor(
        firstStepBuilder: LoanFirstStepBuilder,
        usersRepository: UsersRepository,
        loanOffersRepository: LoanOffersRepository,
        private val router: ApplicationRouter
) : BaseViewModel() {

    val enterAnimationPlayed: BehaviorSubject<Boolean> = BehaviorSubject.createDefault(false)
    val loanBuilderState: BehaviorSubject<LoanFirstStepBuilder> = BehaviorSubject.createDefault(firstStepBuilder)

    val me: BehaviorSubject<User> = BehaviorSubject.create()

    val offers: BehaviorSubject<List<OfferItem>> = BehaviorSubject.create()

    val offerItemClicks: PublishSubject<SharedElementListItem<OfferItem.Offer>> = PublishSubject.create()

    init {
        usersRepository.me().map(UserMapper::map)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { me.onNext(it) }
                .disposeOnCleared()

        Observables.combineLatest(
                loanOffersRepository.observeStandardOffers(),
                loanOffersRepository.observeSpecialOffers()
        )
                .map { (standard, special) -> OfferItemListCreator.mapStandardWithSpecial(standard, special) }
                .observeOn(AndroidSchedulers.mainThread())
                .startWith(loanOffersRepository.observeStandardOffers().map(OfferItemListCreator::mapStandard))
                .subscribe { offers.onNext(it) }
                .disposeOnCleared()

        offerItemClicks.throttleFirst(250, TimeUnit.MILLISECONDS)
                .subscribe {
                    with(it) {
                        router.navigateToOfferDetailsScreen(
                                LoanSecondStepBuilder(
                                        firstStepBuilder,
                                        item.offerGuid,
                                        item.offerName,
                                        item.offerIconUrl,
                                        item.offerMonthlyPayment,
                                        item.interestRateFrom,
                                        item.interestRateTo,
                                        item.interestRateOld,
                                        item.isSpecial
                                ), view, transitionName)
                    }
                }
                .disposeOnCleared()
    }
}