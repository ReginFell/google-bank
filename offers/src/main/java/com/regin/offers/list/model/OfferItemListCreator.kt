package com.regin.offers.list.model

import com.regin.googlebank.core.data.models.OfferResource
import com.regin.offers.R

object OfferItemListCreator {

    fun mapStandard(standardOffers: List<OfferResource>): List<OfferItem> {
        return mutableListOf<OfferItem>().apply {
            add(OfferItem.Title(R.string.special_for_you))
            add(OfferItem.Progress)
            add(OfferItem.Title(R.string.standard_offers))
            standardOffers.forEach {
                add(
                        mapToOffer(
                                it
                        )
                )
            }
        }
    }

    fun mapStandardWithSpecial(
            standardOffers: List<OfferResource>,
            specialOffers: List<OfferResource>
    ): List<OfferItem> {
        return mutableListOf<OfferItem>().apply {
            add(OfferItem.Title(R.string.special_for_you))
            specialOffers.forEach { add(mapToOffer(it, true)) }
            add(OfferItem.Title(R.string.standard_offers))
            standardOffers.forEach {
                add(mapToOffer(it))
            }
        }
    }

    private fun mapToOffer(offerResource: OfferResource, isSpecial: Boolean = false): OfferItem.Offer =
            OfferItem.Offer(
                    offerResource.guid,
                    offerResource.offerName,
                    offerResource.offerIconUrl,
                    offerResource.offerMonthlyPayment,
                    offerResource.interestRateFrom,
                    offerResource.interestRateTo,
                    offerResource.interestRateOld,
                    isSpecial
            )
}