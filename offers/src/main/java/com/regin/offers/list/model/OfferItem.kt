package com.regin.offers.list.model

import androidx.annotation.StringRes

sealed class OfferItem(val viewType: Int) {

    data class Title(@StringRes val title: Int) : OfferItem(ITEM_TYPE_TITLE)
    data class Offer(
            val offerGuid: String,
            val offerName: String,
            val offerIconUrl: String,
            val offerMonthlyPayment: Float,
            val interestRateFrom: Float,
            val interestRateTo: Float?,
            val interestRateOld: Float?,
            val isSpecial: Boolean
    ) : OfferItem(ITEM_TYPE_CONTENT)

    object Progress : OfferItem(ITEM_TYPE_PROGRESS)

    companion object {
        const val ITEM_TYPE_TITLE = 0
        const val ITEM_TYPE_CONTENT = 1
        const val ITEM_TYPE_PROGRESS = 2
    }
}
