package com.regin.offers.list

import com.regin.googlebank.core.ui.models.loan.LoanFirstStepBuilder
import dagger.Module
import dagger.Provides

@Module
class OffersModule {

    @Provides
    fun provideLoanGuid(fragment: OffersFragment): LoanFirstStepBuilder =
        fragment.arguments?.getParcelable(OffersFragment.EXTRA_LOAN_FIRST_STEP)!!

}