package com.regin.offers.list

import android.animation.ObjectAnimator
import android.animation.PropertyValuesHolder
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.View.ALPHA
import android.view.View.TRANSLATION_Y
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProviders
import com.regin.googlebank.core.inject.ViewModelFactory
import com.regin.googlebank.core.ui.BaseFragment
import com.regin.googlebank.core.ui.models.loan.LoanFirstStepBuilder
import com.regin.googlebank.core.ui.utils.recycler.SpaceItemDecoration
import com.regin.offers.R
import dagger.android.support.AndroidSupportInjection
import kotlinx.android.synthetic.main.fragment_offers.*
import javax.inject.Inject

class OffersFragment : BaseFragment<OffersViewModel>() {

    @Inject
    lateinit var viewModelFactory: ViewModelFactory<OffersViewModel>

    override val viewModel: OffersViewModel by lazy {
        ViewModelProviders.of(this, viewModelFactory).get(OffersViewModel::class.java)
    }

    private val offersAdapter = OffersAdapter()

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        AndroidSupportInjection.inject(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_offers, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.enterAnimationPlayed
                .filter { !it }
                .subscribe {
                    viewModel.enterAnimationPlayed.onNext(true)
                    val slideAnimation = PropertyValuesHolder.ofFloat(TRANSLATION_Y, 500f, 0f)
                    val fadeAnimation = PropertyValuesHolder.ofFloat(ALPHA, 0.2f, 1f)
                    val animator = ObjectAnimator.ofPropertyValuesHolder(view, slideAnimation, fadeAnimation)
                    animator.start()
                }
                .disposeOnDestroyView()

        offers.adapter = offersAdapter
        offers.addItemDecoration(SpaceItemDecoration(requireContext().resources.getDimensionPixelOffset(R.dimen.grid_normal)))

        viewModel.loanBuilderState
                .subscribe {
                    with(it) {
                        selectedLoan.setLoan(loanTitle, moneyAmount, duration)
                    }
                }
                .disposeOnDestroyView()

        viewModel.me
                .subscribe { userRating.setRating(it.rating) }
                .disposeOnDestroyView()

        viewModel.offers
                .subscribe { offersAdapter.submitList(it) }
                .disposeOnDestroyView()

        offersAdapter.onItemClickedSubject
                .subscribe(viewModel.offerItemClicks)
    }

    companion object {
        const val EXTRA_LOAN_FIRST_STEP = "extra:loan_first_step"

        fun newInstance(loanFirstStepBuilder: LoanFirstStepBuilder): OffersFragment {
            return OffersFragment().apply {
                arguments = Bundle().apply {
                    putParcelable(EXTRA_LOAN_FIRST_STEP, loanFirstStepBuilder)
                }
            }
        }
    }
}