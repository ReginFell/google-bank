package com.regin.offers.details

import com.regin.googlebank.core.inject.TransitionName
import com.regin.googlebank.core.ui.models.loan.LoanSecondStepBuilder
import dagger.Module
import dagger.Provides

@Module
class OfferDetailsModule {

    @Provides
    fun provideLoanGuid(fragment: OfferDetailsFragment): LoanSecondStepBuilder =
        fragment.arguments?.getParcelable(OfferDetailsFragment.EXTRA_LOAN_SECOND_STEP)!!

    @Provides
    @TransitionName
    fun provideTransitionName(fragment: OfferDetailsFragment): String =
        fragment.arguments?.getString(OfferDetailsFragment.EXTRA_TRANSITION_NAME)!!
}