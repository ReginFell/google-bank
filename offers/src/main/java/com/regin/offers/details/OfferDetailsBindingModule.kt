package com.regin.offers.details

import androidx.lifecycle.ViewModel
import com.regin.googlebank.core.inject.ViewModelKey
import dagger.Binds
import dagger.Module
import dagger.android.ContributesAndroidInjector
import dagger.multibindings.IntoMap

@Module
abstract class OfferDetailsBindingModule {

    @ContributesAndroidInjector(modules = [OfferDetailsModule::class])
    abstract fun bindLoanOfferDetailsFragment(): OfferDetailsFragment

    @Binds
    @IntoMap
    @ViewModelKey(OfferDetailsViewModel::class)
    abstract fun bindLoanOffersViewModel(viewModel: OfferDetailsViewModel): ViewModel

}