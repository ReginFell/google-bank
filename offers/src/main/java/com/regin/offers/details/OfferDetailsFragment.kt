package com.regin.offers.details

import android.content.Context
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.jakewharton.rxbinding2.view.clicks
import com.jakewharton.rxbinding2.widget.textChanges
import com.regin.googlebank.core.inject.TransitionName
import com.regin.googlebank.core.inject.ViewModelFactory
import com.regin.googlebank.core.ui.BaseFragment
import com.regin.googlebank.core.ui.extensions.formatAsMoney
import com.regin.googlebank.core.ui.models.loan.LoanSecondStepBuilder
import com.regin.googlebank.core.ui.utils.*
import com.regin.googlebank.core.ui.views.showWithScale
import com.regin.offers.R
import dagger.android.support.AndroidSupportInjection
import io.reactivex.android.schedulers.AndroidSchedulers
import kotlinx.android.synthetic.main.fragment_offer_details.*
import kotlinx.android.synthetic.main.item_offer.*
import java.util.concurrent.TimeUnit
import javax.inject.Inject
import kotlinx.android.synthetic.main.item_offer.interestRate as interestRateView
import kotlinx.android.synthetic.main.item_offer.monthlyPayment as monthlyPaymentView

class OfferDetailsFragment : BaseFragment<OfferDetailsViewModel>() {

    @Inject
    lateinit var viewModelFactory: ViewModelFactory<OfferDetailsViewModel>

    @Inject
    @field:TransitionName
    lateinit var transitionName: String

    override val viewModel: OfferDetailsViewModel by lazy {
        ViewModelProviders.of(this, viewModelFactory).get(OfferDetailsViewModel::class.java)
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        AndroidSupportInjection.inject(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_offer_details, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        offer.transitionName = transitionName
        AmbientShadowCompat.setOutlineAmbientShadowColor(applyForLoan, R.color.green_accent_color)

        val adapter = OffersInfoAdapter()

        adapter.registerAdapterDataObserver(object : RecyclerView.AdapterDataObserver() {
            override fun onItemRangeInserted(positionStart: Int, itemCount: Int) {
                scrollContainer.post {
                    scrollContainer.smoothScrollTo(0, scrollContainer.getChildAt(0).height)
                }
            }
        })

        chat.adapter = adapter

        viewModel.loanBuilderState
            .subscribe { renderLoanBuilderState(it) }
            .disposeOnDestroyView()

        viewModel.me
            .subscribe { userRating.setRating(it.rating) }
            .disposeOnDestroyView()

        viewModel.offers
            .subscribe { adapter.submitList(it) }
            .disposeOnDestroyView()

        send.clicks()
            .throttleLast(250, TimeUnit.MILLISECONDS)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                viewModel.sendMessage(input.text.toString())
                input.text = null
            }
            .disposeOnDestroyView()

        input.textChanges()
            .subscribe { send.isEnabled = it.isNotBlank() }
            .disposeOnDestroyView()

        adapter.onItemClickedSubject
            .subscribe {
                applyForLoan.showWithScale()
                viewModel.onAppliedToLoan()
            }
            .disposeOnDestroyView()
    }

    private fun renderLoanBuilderState(secondStepBuilder: LoanSecondStepBuilder) {
        with(secondStepBuilder) {
            with(firstStepBuilder) {
                selectedLoan.setLoan(loanTitle, moneyAmount, duration)
            }
            Glide.with(requireContext())
                .load(offerIconUrl)
                .apply(RequestOptions().circleCrop())
                .into(offerIcon)

            title.text = offerName
            monthlyPaymentView.text = offerMonthlyPayment.formatAsMoney()
            interestRateView.text = buildInterest(interestRateFrom, interestRateTo, interestRateOld)

            if (isSpecial) {
                monthlyPaymentView.setTextColor(ContextCompat.getColor(requireContext(), R.color.special_offers_color))
                interestRateView.setTextColor(ContextCompat.getColor(requireContext(), R.color.special_offers_color))
            } else {
                monthlyPaymentView.setTextColor(ContextCompat.getColor(requireContext(), R.color.textColorBlack))
                interestRateView.setTextColor(ContextCompat.getColor(requireContext(), R.color.textColorBlack))
            }
        }
    }

    private fun buildInterest(interestFrom: Float, interestTo: Float?, interestOld: Float?): CharSequence {
        return when {
            interestTo == null && interestOld == null -> interestFrom.toString()
            interestTo == null -> spannable { bold("$interestFrom ") + strike(interestOld.toString()) }
            else -> "$interestFrom - $interestTo"
        }
    }

    companion object {
        const val EXTRA_LOAN_SECOND_STEP = "extra:loan_second_step"
        const val EXTRA_TRANSITION_NAME = "extra:transition_name"

        fun newInstance(secondStepBuilder: LoanSecondStepBuilder, transitionName: String): OfferDetailsFragment {
            return OfferDetailsFragment().apply {
                arguments = Bundle().apply {
                    putParcelable(EXTRA_LOAN_SECOND_STEP, secondStepBuilder)
                    putString(EXTRA_TRANSITION_NAME, transitionName)
                }
            }
        }
    }
}