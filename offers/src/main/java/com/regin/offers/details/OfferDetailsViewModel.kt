package com.regin.offers.details

import com.regin.googlebank.core.data.ChatRepository
import com.regin.googlebank.core.data.UsersRepository
import com.regin.googlebank.core.ui.ApplicationRouter
import com.regin.googlebank.core.ui.BaseViewModel
import com.regin.googlebank.core.ui.models.loan.LoanSecondStepBuilder
import com.regin.googlebank.core.ui.models.user.model.User
import com.regin.googlebank.core.ui.models.user.model.UserMapper
import com.regin.offers.details.model.OfferInfoItem
import com.regin.offers.details.model.OfferInfoItemListCreator
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.subjects.BehaviorSubject
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class OfferDetailsViewModel @Inject constructor(
        secondStepBuilder: LoanSecondStepBuilder,
        usersRepository: UsersRepository,
        private val router: ApplicationRouter,
        private val chatRepository: ChatRepository
) : BaseViewModel() {

    val loanBuilderState: BehaviorSubject<LoanSecondStepBuilder> = BehaviorSubject.createDefault(secondStepBuilder)

    val me: BehaviorSubject<User> = BehaviorSubject.create()

    val offers: BehaviorSubject<List<OfferInfoItem>> = BehaviorSubject.create()

    init {
        usersRepository.me().map(UserMapper::map)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { me.onNext(it) }
                .disposeOnCleared()

        chatRepository.observeChat(secondStepBuilder.offerGuid)
                .map(OfferInfoItemListCreator::map)
                .subscribe { offers.onNext(it) }
                .disposeOnCleared()
    }

    fun sendMessage(text: String) {
        chatRepository.sendMessage(text)
                .subscribe()
                .disposeOnCleared()
    }

    fun onAppliedToLoan() {
        Observable.interval(2, TimeUnit.SECONDS)
                .take(1)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { router.backToRoot() }
                .disposeOnCleared()
    }
}