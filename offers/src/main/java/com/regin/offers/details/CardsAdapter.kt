package com.regin.offers.details

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.regin.googlebank.core.data.models.Card
import com.regin.googlebank.core.data.models.CardType
import com.regin.googlebank.core.ui.extensions.formatAsMoney
import com.regin.offers.R
import io.reactivex.subjects.PublishSubject
import kotlinx.android.synthetic.main.item_card.view.*

class CardsAdapter(private val onItemClickedSubject: PublishSubject<Card>) : ListAdapter<Card, CardsAdapter.ViewHolder>(ItemCallback) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_card, parent, false)
        return ViewHolder(view)
                .apply {
                    itemView.setOnClickListener {
                        if (adapterPosition != RecyclerView.NO_POSITION) {
                            onItemClickedSubject.onNext(getItem(adapterPosition))
                        }
                    }
                }
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(card: Card) {
            with(itemView) {
                container.setCardBackgroundColor(card.cardColor)

                Glide.with(this)
                        .load(card.bankIcon)
                        .apply(RequestOptions().circleCrop())
                        .into(bankLogo)

                accountName.text = card.title
                moneyAmount.text = card.moneyAmount.formatAsMoney()
                cardNumber.text = card.cardNumber
                expires.text = itemView.context.getString(R.string.card_expires, card.expiredDate)

                when (card.cardType) {
                    CardType.VISA -> type.setImageResource(R.drawable.ic_visa)
                }
            }
        }
    }

    private object ItemCallback : DiffUtil.ItemCallback<Card>() {
        override fun areItemsTheSame(oldListItem: Card, newListItem: Card): Boolean {
            return oldListItem.guid == newListItem.guid
        }

        override fun areContentsTheSame(oldListItem: Card, newListItem: Card): Boolean {
            return oldListItem == newListItem
        }
    }
}
