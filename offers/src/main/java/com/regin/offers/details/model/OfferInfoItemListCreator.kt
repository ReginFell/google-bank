package com.regin.offers.details.model

import com.regin.googlebank.core.data.models.Message
import com.regin.offers.R

object OfferInfoItemListCreator {

    fun map(messages: List<Message>): List<OfferInfoItem> {
        return mutableListOf<OfferInfoItem>().apply {
            add(OfferInfoItem.Benefit(R.string.safety, R.string.transfer_money_to_your_account))
            add(OfferInfoItem.SmallDivider)
            add(OfferInfoItem.Benefit(R.string.cashback, R.string.from_one_to_n))
            add(OfferInfoItem.SmallDivider)
            add(OfferInfoItem.Benefit(R.string.free, R.string.you_will_receive_a_free_annual))
            add(OfferInfoItem.FullDivider)
            messages.forEach {
                add(when {
                    it is Message.CardChooserMessage -> OfferInfoItem.MessageCards(it)
                    it.isSender -> OfferInfoItem.MessageSender(it)
                    else -> OfferInfoItem.MessageReceiver(it)
                })
            }
        }
    }
}