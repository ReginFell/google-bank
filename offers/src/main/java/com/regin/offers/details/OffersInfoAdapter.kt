package com.regin.offers.details

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.PagerSnapHelper
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.regin.googlebank.core.data.models.Card
import com.regin.googlebank.core.data.models.Message
import com.regin.offers.R
import com.regin.offers.details.model.OfferInfoItem
import io.reactivex.subjects.PublishSubject
import kotlinx.android.synthetic.main.item_message_cards.view.*
import kotlinx.android.synthetic.main.item_message_sender.view.*
import kotlinx.android.synthetic.main.item_offer_benefit.view.*

class OffersInfoAdapter : ListAdapter<OfferInfoItem, RecyclerView.ViewHolder>(ItemCallback) {

    override fun getItemViewType(position: Int): Int {
        return getItem(position).viewType
    }

    val onItemClickedSubject = PublishSubject.create<Card>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            OfferInfoItem.ITEM_TYPE_BENEFIT -> {
                val view = LayoutInflater.from(parent.context).inflate(R.layout.item_offer_benefit, parent, false)
                BenefitViewHolder(view)
            }
            OfferInfoItem.ITEM_TYPE_SMALL_DIVIDER -> {
                val view = LayoutInflater.from(parent.context).inflate(R.layout.item_small_divider, parent, false)
                ProgressViewHolder(view)
            }
            OfferInfoItem.ITEM_TYPE_FULL_DIVIDER -> {
                val view = LayoutInflater.from(parent.context).inflate(R.layout.item_full_divider, parent, false)
                ProgressViewHolder(view)
            }
            OfferInfoItem.ITEM_TYPE_MESSAGE_RECEIVER -> {
                val view = LayoutInflater.from(parent.context).inflate(R.layout.item_message_receiver, parent, false)
                MessageViewHolder(view)
            }
            OfferInfoItem.ITEM_TYPE_MESSAGE_SENDER -> {
                val view = LayoutInflater.from(parent.context).inflate(R.layout.item_message_sender, parent, false)
                MessageViewHolder(view)
            }
            OfferInfoItem.ITEM_TYPE_MESSAGE_CARDS -> {
                val view = LayoutInflater.from(parent.context).inflate(R.layout.item_message_cards, parent, false)
                CardsViewHolder(view)
            }
            else -> throw RuntimeException("Unexpected holder type $viewType")
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        with(getItem(position)) {
            when (this) {
                is OfferInfoItem.Benefit -> {
                    (holder as BenefitViewHolder).bind(this)
                }
                is OfferInfoItem.MessageReceiver -> {
                    (holder as MessageViewHolder).bind(this.message as Message.TextMessage)
                }
                is OfferInfoItem.MessageSender -> {
                    (holder as MessageViewHolder).bind(this.message as Message.TextMessage)
                }
                is OfferInfoItem.MessageCards -> {
                    (holder as CardsViewHolder).bind(this.message)
                }
            }
        }
    }

    private inner class BenefitViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(benefitItem: OfferInfoItem.Benefit) {
            itemView.benefitTitle.setText(benefitItem.titleRes)
            itemView.benefitDescription.setText(benefitItem.descriptionRes)
        }
    }

    private inner class ProgressViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    private inner class MessageViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(message: Message.TextMessage) {
            itemView.text.text = message.text

            Glide.with(itemView)
                    .load(message.userAvatar)
                    .apply(RequestOptions().circleCrop())
                    .into(itemView.avatar)
        }
    }

    private inner class CardsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(message: Message.CardChooserMessage) {
            val adapter = CardsAdapter(onItemClickedSubject)

            itemView.cards.adapter = adapter
            adapter.submitList(message.cards)

            val snapHelper = PagerSnapHelper()
            snapHelper.attachToRecyclerView(itemView.cards)
        }
    }


    private object ItemCallback : DiffUtil.ItemCallback<OfferInfoItem>() {
        override fun areItemsTheSame(oldListItem: OfferInfoItem, newListItem: OfferInfoItem): Boolean {
            return oldListItem == newListItem
        }

        override fun areContentsTheSame(oldListItem: OfferInfoItem, newListItem: OfferInfoItem): Boolean {
            return oldListItem == newListItem
        }
    }
}
