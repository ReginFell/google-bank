package com.regin.offers.details.model

import androidx.annotation.StringRes
import com.regin.googlebank.core.data.models.Message

sealed class OfferInfoItem(val viewType: Int) {
    data class Benefit(@StringRes val titleRes: Int, @StringRes val descriptionRes: Int) : OfferInfoItem(ITEM_TYPE_BENEFIT)
    object SmallDivider : OfferInfoItem(ITEM_TYPE_SMALL_DIVIDER)
    object FullDivider : OfferInfoItem(ITEM_TYPE_FULL_DIVIDER)
    data class MessageSender(val message: Message) : OfferInfoItem(ITEM_TYPE_MESSAGE_SENDER)
    data class MessageReceiver(val message: Message) : OfferInfoItem(ITEM_TYPE_MESSAGE_RECEIVER)
    data class MessageCards(val message: Message.CardChooserMessage) : OfferInfoItem(ITEM_TYPE_MESSAGE_CARDS)

    companion object {
        const val ITEM_TYPE_BENEFIT = 0
        const val ITEM_TYPE_SMALL_DIVIDER = 1
        const val ITEM_TYPE_FULL_DIVIDER = 2
        const val ITEM_TYPE_MESSAGE_SENDER = 3
        const val ITEM_TYPE_MESSAGE_RECEIVER = 4
        const val ITEM_TYPE_MESSAGE_CARDS = 5
    }
}