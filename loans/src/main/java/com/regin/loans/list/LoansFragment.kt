package com.regin.loans.list

import android.content.Context
import android.os.Bundle
import android.view.*
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.regin.googlebank.core.inject.ViewModelFactory
import com.regin.googlebank.core.ui.BaseFragment
import com.regin.googlebank.core.ui.utils.recycler.SpaceItemDecoration
import com.regin.loans.R
import dagger.android.support.AndroidSupportInjection
import kotlinx.android.synthetic.main.fragment_loans.*
import javax.inject.Inject

class LoansFragment : BaseFragment<LoansViewModel>() {

    @Inject
    lateinit var viewModelFactory: ViewModelFactory<LoansViewModel>

    override val viewModel: LoansViewModel by lazy {
        ViewModelProviders.of(this, viewModelFactory).get(LoansViewModel::class.java)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        AndroidSupportInjection.inject(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_loans, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val adapter = LoansAdapter()

        loans.adapter = adapter
        loans.addItemDecoration(SpaceItemDecoration(requireContext().resources.getDimensionPixelOffset(R.dimen.grid_normal)))

        adapter.onItemClickedSubject
            .subscribe(viewModel.loanListItemClicks)

        viewModel.loans.observe(this, Observer { adapter.submitList(it) })
    }
}