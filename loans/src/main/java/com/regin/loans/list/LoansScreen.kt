package com.regin.loans.list

import com.regin.googlebank.core.ui.utils.navigation.SupportAppScreen

class LoansScreen : SupportAppScreen() {

    override val fragment = LoansFragment()

}