package com.regin.loans.list.model

import com.regin.googlebank.core.data.models.LoanResource
import com.regin.googlebank.core.data.models.LoanType
import com.regin.loans.R

object LoanListItemMapper {

    fun map(loanResources: List<LoanResource>): List<LoanListItem> = loanResources.map(::map)

    fun map(loanResource: LoanResource): LoanListItem = with(loanResource) {
        LoanListItem(
            guid = guid,
            title = title,
            subtitle = subtitle,
            description = description,
            loanDrawableRes = when (loanType) {
                LoanType.REFINANCE -> R.drawable.loan_bg_refinance
                LoanType.CONSUMER -> R.drawable.loan_bg_consumer_loan
                LoanType.AUTO -> R.drawable.loan_bg_auto
                LoanType.MORTGAGE -> R.drawable.loan_bg_mortage
            }
        )
    }
}