package com.regin.loans.list

import androidx.lifecycle.ViewModel
import com.regin.googlebank.core.inject.ViewModelKey
import dagger.Binds
import dagger.Module
import dagger.android.ContributesAndroidInjector
import dagger.multibindings.IntoMap

@Module
abstract class LoansBindingModule {

    @ContributesAndroidInjector
    abstract fun bindLoansFragment(): LoansFragment

    @Binds
    @IntoMap
    @ViewModelKey(LoansViewModel::class)
    abstract fun bindLoansViewModel(viewModel: LoansViewModel): ViewModel

}