package com.regin.loans.list.model

import androidx.annotation.DrawableRes

data class LoanListItem(
    val guid: String,
    val title: String,
    val subtitle: String,
    val description: String,
    @DrawableRes val loanDrawableRes: Int
)
