package com.regin.loans.list

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.regin.googlebank.core.ui.models.SharedElementListItem
import com.regin.loans.R
import com.regin.loans.list.model.LoanListItem
import io.reactivex.subjects.PublishSubject
import kotlinx.android.synthetic.main.item_loan.view.*

class LoansAdapter : ListAdapter<LoanListItem, LoansAdapter.ViewHolder>(ItemCallback) {

       companion object {
        private const val TRANSITION_TAG = "transition"
    }

    val onItemClickedSubject = PublishSubject.create<SharedElementListItem<LoanListItem>>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_loan, parent, false)
        return ViewHolder(view)
                .apply {
                    itemView.setOnClickListener {
                        if (adapterPosition != RecyclerView.NO_POSITION) {
                            onItemClickedSubject.onNext(
                                    SharedElementListItem(
                                            getItem(adapterPosition),
                                            it,
                                            buildTransactionName(adapterPosition))
                            )
                        }
                    }
                }
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(loanListItem: LoanListItem) {
            with(itemView) {
                title.text = loanListItem.title
                subtitle.text = loanListItem.subtitle
                description.text = loanListItem.description
                image.setImageResource(loanListItem.loanDrawableRes)

                transitionName = buildTransactionName(adapterPosition)
            }
        }
    }

    private fun buildTransactionName(position: Int): String {
        return "$TRANSITION_TAG$position"
    }

    private object ItemCallback : DiffUtil.ItemCallback<LoanListItem>() {
        override fun areItemsTheSame(oldListItem: LoanListItem, newListItem: LoanListItem): Boolean {
            return oldListItem.guid == newListItem.guid
        }

        override fun areContentsTheSame(oldListItem: LoanListItem, newListItem: LoanListItem): Boolean {
            return oldListItem == newListItem
        }
    }
}
