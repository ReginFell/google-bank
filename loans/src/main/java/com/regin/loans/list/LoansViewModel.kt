package com.regin.loans.list

import androidx.lifecycle.MutableLiveData
import com.regin.googlebank.core.data.LoansRepository
import com.regin.googlebank.core.ui.ApplicationRouter
import com.regin.googlebank.core.ui.BaseViewModel
import com.regin.googlebank.core.ui.models.SharedElementListItem
import com.regin.googlebank.core.ui.utils.lifecycle.LocalError
import com.regin.loans.list.model.LoanListItem
import com.regin.loans.list.model.LoanListItemMapper
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.subjects.PublishSubject
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class LoansViewModel @Inject constructor(
    loansRepository: LoansRepository,
    private val router: ApplicationRouter
) : BaseViewModel() {

    val loans: MutableLiveData<List<LoanListItem>> = MutableLiveData()

    val loanListItemClicks: PublishSubject<SharedElementListItem<LoanListItem>> = PublishSubject.create()

    init {
        loansRepository.observeLoans()
            .map(LoanListItemMapper::map)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ loans.value = it }, { localErrorLiveData.value = LocalError(it) })
            .disposeOnCleared()

        loanListItemClicks.throttleFirst(250, TimeUnit.MILLISECONDS)
            .subscribe {
                router.navigateToLoanDetails(it.item.guid, it.view, it.transitionName)
            }
            .disposeOnCleared()
    }
}