package com.regin.loans.details

import android.animation.ValueAnimator
import android.content.Context
import android.os.Bundle
import android.transition.Fade
import android.transition.TransitionManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AccelerateDecelerateInterpolator
import androidx.lifecycle.ViewModelProviders
import com.jakewharton.rxbinding2.view.clicks
import com.regin.googlebank.core.data.models.LoanDurationResource
import com.regin.googlebank.core.inject.TransitionName
import com.regin.googlebank.core.inject.ViewModelFactory
import com.regin.googlebank.core.ui.BaseFragment
import com.regin.googlebank.core.ui.views.showWithScale
import com.regin.loans.R
import com.regin.loans.details.model.Loan
import com.regin.moneysliderview.changes
import dagger.android.support.AndroidSupportInjection
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import kotlinx.android.synthetic.main.fragment_loan_details.*
import kotlinx.android.synthetic.main.layout_loan_time.*
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class LoanDetailsFragment : BaseFragment<LoanDetailsViewModel>() {

    @Inject
    lateinit var viewModelFactory: ViewModelFactory<LoanDetailsViewModel>

    @Inject
    @field:TransitionName
    lateinit var transitionName: String

    override val viewModel: LoanDetailsViewModel by lazy {
        ViewModelProviders.of(this, viewModelFactory).get(LoanDetailsViewModel::class.java)
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        AndroidSupportInjection.inject(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_loan_details, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        selectedLoanCard.transitionName = transitionName

        onEnterTransition()

        cardSlider.changes()
                .throttleLast(250, TimeUnit.MILLISECONDS)
                .subscribe(viewModel.moneyAmountChanges)

        searchLoanSelected.clicks()
                .subscribe(viewModel.loanSearchClicks)

        viewModel.moneyAmountChanges
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { cardSlider.setValue(it) }
                .disposeOnDestroyView()

        viewModel.durationClicks
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { selectDuration(it) }
                .disposeOnDestroyView()

        viewModel.me
                .subscribe { userRating.setRating(it.rating) }
                .disposeOnDestroyView()

        viewModel.selectedLoan
                .subscribe { renderSelectedLoan(it) }
                .disposeOnDestroyView()

        viewModel.nextStepAvailable
                .filter { it }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { searchLoanSelectedContainer.showWithScale() }
                .disposeOnDestroyView()
    }

    private fun renderSelectedLoan(loan: Loan) {
        with(loan) {
            selectedLoan.text = selectedLoanTitle

            smallDurationText.text = smallDuration.duration.toString()
            smallDurationSelectedText.text = smallDuration.duration.toString()

            Observable.merge(
                    listOf(small.clicks().map { smallDuration },
                            regular.clicks().map { regularDuration },
                            large.clicks().map { largeDuration })
            )
                    .distinctUntilChanged()
                    .throttleLast(250, TimeUnit.MILLISECONDS)
                    .subscribe(viewModel.durationClicks)

            regularDurationText.text = regularDuration.duration.toString()
            regularDurationSelectedText.text = regularDuration.duration.toString()

            largeDurationText.text = largeDuration.duration.toString()
            largeDurationSelectedText.text = largeDuration.duration.toString()
        }
    }

    private fun selectDuration(duration: LoanDurationResource) {
        TransitionManager.beginDelayedTransition(container, Fade())

        smallSelected.visibility = View.INVISIBLE
        regularSelected.visibility = View.INVISIBLE
        largeSelected.visibility = View.INVISIBLE

        when (duration) {
            is LoanDurationResource.Small -> smallSelected.showWithScale()
            is LoanDurationResource.Regular -> regularSelected.showWithScale()
            is LoanDurationResource.Large -> largeSelected.showWithScale()
        }
    }

    private fun onEnterTransition() {
        val fadeViews = listOf(userRating, cardSliderCard, cardLoanPeriod)

        ValueAnimator.ofFloat(0f, 1f)
                .apply {
                    duration = 750
                    interpolator = AccelerateDecelerateInterpolator()

                    addUpdateListener { animatorValue ->
                        fadeViews.forEach { it.alpha = animatorValue.animatedValue as Float }
                    }
                    start()
                }
    }

    companion object {
        const val EXTRA_LOAN_GUID = "extra:loan_guid"
        const val EXTRA_TRANSITION_NAME = "extra:transition_name"

        fun newInstance(loanGuid: String, transitionName: String): LoanDetailsFragment {
            return LoanDetailsFragment().apply {
                arguments = Bundle().apply {
                    putString(EXTRA_LOAN_GUID, loanGuid)
                    putString(EXTRA_TRANSITION_NAME, transitionName)
                }
            }
        }
    }
}
