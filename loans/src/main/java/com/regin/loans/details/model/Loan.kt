package com.regin.loans.details.model

import com.regin.googlebank.core.data.models.LoanDurationResource

data class Loan(
    val loanGuid: String,
    val selectedLoanTitle: String,
    val smallDuration: LoanDurationResource.Small,
    val regularDuration: LoanDurationResource.Regular,
    val largeDuration: LoanDurationResource.Large
)