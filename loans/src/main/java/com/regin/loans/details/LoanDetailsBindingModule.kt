package com.regin.loans.details

import androidx.lifecycle.ViewModel
import com.regin.googlebank.core.inject.ViewModelKey
import dagger.Binds
import dagger.Module
import dagger.android.ContributesAndroidInjector
import dagger.multibindings.IntoMap

@Module
abstract class LoanDetailsBindingModule {

    @ContributesAndroidInjector(modules = [LoanDetailsModule::class])
    abstract fun bindLoansFragment(): LoanDetailsFragment

    @Binds
    @IntoMap
    @ViewModelKey(LoanDetailsViewModel::class)
    abstract fun bindLoansViewModel(viewModel: LoanDetailsViewModel): ViewModel

}