package com.regin.loans.details

import com.regin.googlebank.core.inject.LoanGuid
import com.regin.googlebank.core.inject.TransitionName
import dagger.Module
import dagger.Provides

@Module
class LoanDetailsModule {

    @Provides
    @LoanGuid
    fun provideLoanGuid(fragment: LoanDetailsFragment): String =
        fragment.arguments?.getString(LoanDetailsFragment.EXTRA_LOAN_GUID)!!

    @Provides
    @TransitionName
    fun provideTransitionName(fragment: LoanDetailsFragment): String =
        fragment.arguments?.getString(LoanDetailsFragment.EXTRA_TRANSITION_NAME)!!
}