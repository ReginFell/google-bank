package com.regin.loans.details.model

import com.regin.googlebank.core.data.models.LoanResource

object LoanMapper {

    fun map(loanResource: LoanResource): Loan = Loan(
        loanGuid = loanResource.guid,
        selectedLoanTitle = loanResource.title,
        smallDuration = loanResource.smallDuration,
        regularDuration = loanResource.regularDuration,
        largeDuration = loanResource.largeDuration
    )

}