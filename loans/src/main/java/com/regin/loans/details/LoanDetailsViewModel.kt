package com.regin.loans.details

import com.regin.googlebank.core.data.LoansRepository
import com.regin.googlebank.core.data.UsersRepository
import com.regin.googlebank.core.data.models.LoanDurationResource
import com.regin.googlebank.core.inject.LoanGuid
import com.regin.googlebank.core.ui.ApplicationRouter
import com.regin.googlebank.core.ui.BaseViewModel
import com.regin.googlebank.core.ui.models.loan.LoanFirstStepBuilder
import com.regin.googlebank.core.ui.models.user.model.User
import com.regin.googlebank.core.ui.models.user.model.UserMapper
import com.regin.loans.details.model.Loan
import com.regin.loans.details.model.LoanMapper
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.Observables
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.PublishSubject
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class LoanDetailsViewModel @Inject constructor(
    @LoanGuid private val loanGuid: String,
    private val router: ApplicationRouter,
    usersRepository: UsersRepository,
    loansRepository: LoansRepository
) : BaseViewModel() {

    val me: BehaviorSubject<User> = BehaviorSubject.create()
    val selectedLoan: BehaviorSubject<Loan> = BehaviorSubject.create()

    val moneyAmountChanges: BehaviorSubject<Float> = BehaviorSubject.create()
    val durationClicks: BehaviorSubject<LoanDurationResource> = BehaviorSubject.create()

    val nextStepAvailable: BehaviorSubject<Boolean> = BehaviorSubject.createDefault(false)

    val loanSearchClicks: PublishSubject<Unit> = PublishSubject.create()

    init {
        usersRepository.me().map(UserMapper::map)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { me.onNext(it) }
            .disposeOnCleared()

        loansRepository.observeLoan(loanGuid).map(LoanMapper::map)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { selectedLoan.onNext(it) }
            .disposeOnCleared()

        Observables.combineLatest(moneyAmountChanges, durationClicks)
            .take(1)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { nextStepAvailable.onNext(true) }
            .disposeOnCleared()

        loanSearchClicks.throttleLast(250, TimeUnit.MILLISECONDS)
            .switchMap {
                Observables.combineLatest(moneyAmountChanges, durationClicks, selectedLoan).take(1)
            }
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { (moneyAmount, duration, loan) ->
                router.navigateToOffersScreen(
                        LoanFirstStepBuilder(loanGuid, loan.selectedLoanTitle, moneyAmount, duration.duration)
                )
            }
            .disposeOnCleared()
    }
}