package com.regin.ambientshadowview

import android.content.Context
import android.graphics.*
import android.graphics.drawable.Drawable
import android.renderscript.Allocation
import android.renderscript.Element
import android.renderscript.RenderScript
import android.renderscript.ScriptIntrinsicBlur
import android.util.AttributeSet
import android.widget.FrameLayout
import androidx.core.content.ContextCompat
import androidx.interpolator.view.animation.FastOutSlowInInterpolator

class AmbientShadowView @JvmOverloads constructor(
        context: Context,
        attrs: AttributeSet? = null,
        defStyleAttrs: Int = 0
) : FrameLayout(context, attrs, defStyleAttrs) {

    private var renderDrawable: Drawable? = null
        set(value) {
            field = value
            setIconBounds()
            createShadows()
        }

    private var bigBlurShadow: Bitmap? = null
    private var smallBlurShadow: Bitmap? = null
    private var shadowDrawable: Drawable? = null
    private var smallBlurShadowAlpha = 255
    private var bigBlurShadowAlpha = 255
    private var shadowTranslate = 0f
    private var constantShadowTranslationY = 8f
    private var variableShadowTranslationY = 16f
    private var scaleDown = 0.85f
    private var bigBlurRadius = 24f
    private var smallBlurRadius = 8f
    private var padding = bigBlurRadius.toInt()
    private val iconBounds = Rect()
    private val shadowBounds = RectF()
    private val shadowPaint = Paint(Paint.ANTI_ALIAS_FLAG).apply {
        colorFilter = ColorMatrixColorFilter(ColorMatrix().apply {
            setScale(SHADOW_SCALE_RGB, SHADOW_SCALE_RGB, SHADOW_SCALE_RGB, SHADOW_SCALE_ALPHA)
        })
    }

    init {
        val a = context.obtainStyledAttributes(attrs, R.styleable.AmbientShadowView, 0, 0)
        constantShadowTranslationY = a.getDimension(
                R.styleable.AmbientShadowView_constantShadowTranslationY,
                constantShadowTranslationY
        )

        if (a.hasValue(R.styleable.AmbientShadowView_shadowDrawable)) {
            shadowDrawable = ContextCompat.getDrawable(
                    context,
                    a.getResourceId(
                            R.styleable.AmbientShadowView_shadowDrawable, -1))
        }

        constantShadowTranslationY = a.getDimension(
                R.styleable.AmbientShadowView_constantShadowTranslationY,
                constantShadowTranslationY
        )

        variableShadowTranslationY = a.getDimension(
                R.styleable.AmbientShadowView_variableShadowTranslationY,
                variableShadowTranslationY
        )
        scaleDown = a.getFloat(R.styleable.AmbientShadowView_scaleDown, scaleDown).coerceIn(0f, 1f)
        bigBlurRadius = a.getFloat(R.styleable.AmbientShadowView_bigBlurRadius, bigBlurRadius)
                .coerceIn(0f, 25f)
        padding = bigBlurRadius.toInt()
        smallBlurRadius = a.getFloat(R.styleable.AmbientShadowView_smallBlurRadius, smallBlurRadius)
                .coerceIn(0f, 25f)
        a.recycle()

        renderDrawable = shadowDrawable
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        val shadowTop = (h - w) / 2
        val iconTop = shadowTop + padding
        val iconSize = w - padding - padding
        iconBounds.set(padding, iconTop, padding + iconSize, iconTop + iconSize)
        shadowBounds.set(0f, shadowTop.toFloat(), w.toFloat(), (shadowTop + w).toFloat())
        setIconBounds()
        createShadows()
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        val saveCount = canvas.save()
        canvas.translate(shadowBounds.left, shadowBounds.top + shadowTranslate)
        shadowPaint.alpha = bigBlurShadowAlpha
        canvas.drawBitmap(bigBlurShadow, 0f, 0f, shadowPaint)
        shadowPaint.alpha = smallBlurShadowAlpha
        canvas.drawBitmap(smallBlurShadow, 0f, 0f, shadowPaint)
        canvas.restoreToCount(saveCount)
        renderDrawable?.draw(canvas)
    }

    private fun setIconBounds() {
        renderDrawable?.bounds = iconBounds
    }

    private fun createShadows() {
        if (renderDrawable == null || shadowBounds.width() == 0f) return
        if (bigBlurShadow == null) {
            bigBlurShadow = Bitmap.createBitmap(
                    shadowBounds.width().toInt(), shadowBounds.height().toInt(), Bitmap.Config.ARGB_8888
            )
        } else {
            bigBlurShadow?.eraseColor(Color.TRANSPARENT)
        }
        if (smallBlurShadow == null) {
            smallBlurShadow = Bitmap.createBitmap(
                    shadowBounds.width().toInt(), shadowBounds.height().toInt(), Bitmap.Config.ARGB_8888
            )
        } else {
            smallBlurShadow?.eraseColor(Color.TRANSPARENT)
        }
        val c = Canvas()
        createShadow(bigBlurShadow, c, bigBlurRadius)
        createShadow(smallBlurShadow, c, smallBlurRadius)
    }

    private fun createShadow(bitmap: Bitmap?, canvas: Canvas, blurRadius: Float) {
        canvas.setBitmap(bitmap)
        canvas.translate(
                padding - iconBounds.left.toFloat(),
                padding - iconBounds.top.toFloat()
        )
        renderDrawable?.draw(canvas)
        val rs = getRS(context)
        val blur = getBlur(context)
        val input = Allocation.createFromBitmap(rs, bitmap)
        val output = Allocation.createTyped(rs, input.type)
        blur.setRadius(blurRadius.coerceIn(0f, 25f))
        blur.setInput(input)
        blur.forEach(output)
        output.copyTo(bitmap)
        input.destroy()
        output.destroy()
    }

    companion object {
        private const val SHADOW_SCALE_RGB = 0.85f
        private const val SHADOW_SCALE_ALPHA = 0.6f
        private val interpolator = FastOutSlowInInterpolator()

        private var rs: RenderScript? = null
        fun getRS(context: Context): RenderScript {
            if (rs == null) {
                rs = RenderScript.create(context.applicationContext)
            }
            return rs!!
        }

        private var blur: ScriptIntrinsicBlur? = null
        fun getBlur(context: Context): ScriptIntrinsicBlur {
            if (blur == null) {
                val rs = getRS(context)
                blur = ScriptIntrinsicBlur.create(rs, Element.U8_4(rs))
            }
            return blur!!
        }
    }
}