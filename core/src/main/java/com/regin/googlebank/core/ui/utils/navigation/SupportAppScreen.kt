package com.regin.googlebank.core.ui.utils.navigation

import android.content.Context
import android.content.Intent

import androidx.fragment.app.Fragment
import ru.terrakok.cicerone.Screen

abstract class SupportAppScreen : Screen() {

    open val fragment: Fragment? = null

    open fun getActivityIntent(context: Context): Intent? = null

}
