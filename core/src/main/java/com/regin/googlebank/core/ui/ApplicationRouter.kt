package com.regin.googlebank.core.ui

import android.view.View
import com.regin.googlebank.core.ui.models.loan.LoanFirstStepBuilder
import com.regin.googlebank.core.ui.models.loan.LoanSecondStepBuilder

interface ApplicationRouter {

    fun backToRoot()

    fun navigateToLoanDetails(loanGuid: String, sharedView: View, transitionName: String)

    fun navigateToOffersScreen(loanFirstStepBuilder: LoanFirstStepBuilder)

    fun navigateToOfferDetailsScreen(loanSecondStepBuilder: LoanSecondStepBuilder,
                                     sharedView: View,
                                     transitionName: String)
}