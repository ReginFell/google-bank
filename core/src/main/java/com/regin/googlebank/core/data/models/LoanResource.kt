package com.regin.googlebank.core.data.models

data class LoanResource(
    val guid: String,
    val title: String,
    val subtitle: String,
    val description: String,
    val loanType: LoanType,
    val smallDuration: LoanDurationResource.Small,
    val regularDuration: LoanDurationResource.Regular,
    val largeDuration: LoanDurationResource.Large
)

enum class LoanType {
    CONSUMER, REFINANCE, AUTO, MORTGAGE
}