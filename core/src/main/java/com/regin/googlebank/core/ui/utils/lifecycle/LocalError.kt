package com.regin.googlebank.core.ui.utils.lifecycle

data class LocalError(val throwable: Throwable)