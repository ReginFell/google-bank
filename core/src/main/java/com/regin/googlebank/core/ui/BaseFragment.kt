package com.regin.googlebank.core.ui

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.regin.googlebank.core.ui.utils.lifecycle.LocalError
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

abstract class BaseFragment<out T : BaseViewModel> : Fragment() {

    val compositeDisposable: CompositeDisposable by lazy { CompositeDisposable() }

    protected abstract val viewModel: T

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.localErrorLiveData.observe(this, Observer { handleError(it) })
    }

    open fun handleError(localError: LocalError?) {
        localError?.run {
            Toast.makeText(requireContext(), localError.throwable.message, Toast.LENGTH_LONG).show()
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        compositeDisposable.clear()
    }

    protected fun Disposable.disposeOnDestroyView(): Disposable = apply {
        compositeDisposable.add(this)
    }
}