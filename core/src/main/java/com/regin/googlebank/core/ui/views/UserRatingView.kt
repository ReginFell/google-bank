package com.regin.googlebank.core.ui.views

import android.content.Context
import android.util.AttributeSet
import androidx.constraintlayout.widget.ConstraintLayout
import com.regin.core.R
import kotlinx.android.synthetic.main.view_user_rating.view.*

class UserRatingView @JvmOverloads constructor(
        context: Context,
        attrs: AttributeSet? = null,
        defStyleAttrs: Int = 0
) : ConstraintLayout(context, attrs, defStyleAttrs) {

    init {
        inflate(context, R.layout.view_user_rating, this)
    }

    fun setRating(rating: Int) {
        ratingValue.text = context.getString(R.string.rating_value, rating)
    }
}