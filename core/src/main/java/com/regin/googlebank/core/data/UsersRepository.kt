package com.regin.googlebank.core.data

import com.regin.googlebank.core.data.models.UserResource
import io.reactivex.Observable

interface UsersRepository {

    fun me(): Observable<UserResource>

}