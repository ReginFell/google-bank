package com.regin.googlebank.core.ui.utils.navigation

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentTransaction
import androidx.transition.TransitionInflater
import com.regin.googlebank.core.ui.utils.navigation.command.ForwardWithTransition
import ru.terrakok.cicerone.commands.Command

class ApplicationNavigator(
        private val activity: FragmentActivity,
        containerId: Int
) : SupportAppNavigator(activity, containerId) {

    private val transitionInflater by lazy { TransitionInflater.from(activity) }

    override fun setupFragmentTransaction(command: Command?, currentFragment: Fragment?, nextFragment: Fragment?, fragmentTransaction: FragmentTransaction) {
        when (command) {
            is ForwardWithTransition -> {
                with(command.screen) {
                    sharedElement?.let {
                        currentFragment?.sharedElementReturnTransition = transitionInflater.inflateTransition(sharedElementReturnTransaction)
                        currentFragment?.exitTransition = transitionInflater.inflateTransition(fromExitTransition)

                        nextFragment?.sharedElementEnterTransition = transitionInflater.inflateTransition(sharedElementEnterTransition)
                        nextFragment?.exitTransition = transitionInflater.inflateTransition(fromExitTransition)

                        fragmentTransaction.addSharedElement(it, it.transitionName)

                    }
                }
            }
        }
    }
}