package com.regin.googlebank.core.ui.utils.navigation.screen

import android.view.View
import androidx.annotation.TransitionRes
import com.regin.googlebank.core.ui.utils.navigation.SupportAppScreen

abstract class TransitionScreen : SupportAppScreen() {

    open val sharedElement: View? = null

    @TransitionRes open val sharedElementReturnTransaction: Int = android.R.transition.no_transition
    @TransitionRes open val fromExitTransition: Int = android.R.transition.no_transition
    @TransitionRes open val sharedElementEnterTransition: Int = android.R.transition.no_transition
    @TransitionRes open val toExitTransition: Int = android.R.transition.no_transition

}