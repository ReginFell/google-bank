package com.regin.googlebank.core.data

import com.regin.googlebank.core.data.models.LoanResource
import io.reactivex.Observable

interface LoansRepository {

    fun observeLoans(): Observable<List<LoanResource>>

    fun observeLoan(guid: String): Observable<LoanResource>

}