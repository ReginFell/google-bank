package com.regin.googlebank.core.ui.views

import android.animation.ValueAnimator
import android.view.View
import android.view.animation.AccelerateDecelerateInterpolator

fun View.showWithScale() {
    visibility = View.VISIBLE

    ValueAnimator.ofFloat(0f, 1f)
            .apply {
                interpolator = AccelerateDecelerateInterpolator()

                addUpdateListener { animatorValue ->
                    scaleX = animatorValue.animatedValue as Float
                    scaleY = animatorValue.animatedValue as Float
                }
                start()
            }
}