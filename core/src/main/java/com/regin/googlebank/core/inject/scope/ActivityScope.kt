package com.regin.googlebank.core.inject.scope

import javax.inject.Scope

@Scope
annotation class ActivityScope