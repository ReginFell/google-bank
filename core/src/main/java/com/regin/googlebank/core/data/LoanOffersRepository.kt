package com.regin.googlebank.core.data

import com.regin.googlebank.core.data.models.OfferResource
import io.reactivex.Observable

interface LoanOffersRepository {

    fun observeStandardOffers(): Observable<List<OfferResource>>

    fun observeSpecialOffers(): Observable<List<OfferResource>>

}