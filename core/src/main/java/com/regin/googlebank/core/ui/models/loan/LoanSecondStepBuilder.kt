package com.regin.googlebank.core.ui.models.loan

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class LoanSecondStepBuilder(
        val firstStepBuilder: LoanFirstStepBuilder,
        val offerGuid: String,
        val offerName: String,
        val offerIconUrl: String,
        val offerMonthlyPayment: Float,
        val interestRateFrom: Float,
        val interestRateTo: Float?,
        val interestRateOld: Float?,
        val isSpecial: Boolean
) : Parcelable