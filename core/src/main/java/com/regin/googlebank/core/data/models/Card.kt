package com.regin.googlebank.core.data.models

data class Card(
        val guid: String,
        val title: String,
        val cardColor: Int,
        val moneyAmount: Float,
        val bankIcon: String,
        val cardNumber: String,
        val expiredDate: String,
        val cardType: CardType
)

enum class CardType {
    VISA
}