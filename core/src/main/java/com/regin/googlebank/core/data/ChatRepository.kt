package com.regin.googlebank.core.data

import com.regin.googlebank.core.data.models.Message
import io.reactivex.Completable
import io.reactivex.Observable

interface ChatRepository {

    fun observeChat(offerGuid: String): Observable<List<Message>>

    fun sendMessage(text: String): Completable

}