package com.regin.googlebank.core.inject

import javax.inject.Qualifier

@Qualifier
annotation class LoanGuid

@Qualifier
annotation class TransitionName

