package com.regin.googlebank.core.ui.extensions

import java.text.DecimalFormat
import java.text.NumberFormat

fun Float.formatAsMoney(withSymbol: Boolean = true): String {
    val formatter = NumberFormat.getCurrencyInstance()
    val decimalFormatSymbols = (formatter as DecimalFormat).decimalFormatSymbols
    decimalFormatSymbols.currencySymbol = if (withSymbol) "$" else ""
    formatter.decimalFormatSymbols = decimalFormatSymbols
    return formatter.format(this)
}