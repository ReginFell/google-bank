package com.regin.googlebank.core.ui

import androidx.lifecycle.ViewModel
import com.regin.googlebank.core.ui.utils.lifecycle.LocalError
import com.regin.googlebank.core.ui.utils.lifecycle.SingleLiveEvent
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

abstract class BaseViewModel : ViewModel() {

    val localErrorLiveData: SingleLiveEvent<LocalError> = SingleLiveEvent()

    val compositeDisposable: CompositeDisposable by lazy { CompositeDisposable() }

    override fun onCleared() {
        compositeDisposable.clear()
    }

    protected fun Disposable.disposeOnCleared(): Disposable = apply {
        compositeDisposable.add(this)
    }
}