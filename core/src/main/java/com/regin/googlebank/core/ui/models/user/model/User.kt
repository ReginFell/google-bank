package com.regin.googlebank.core.ui.models.user.model

data class User(val userGuid: String, val rating: Int)