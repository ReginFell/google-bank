package com.regin.googlebank.core.ui.utils.navigation.command

import com.regin.googlebank.core.ui.utils.navigation.screen.TransitionScreen
import ru.terrakok.cicerone.commands.Forward

class ForwardWithTransition(private val screen: TransitionScreen) : Forward(screen) {

    override fun getScreen(): TransitionScreen {
        return screen
    }
}