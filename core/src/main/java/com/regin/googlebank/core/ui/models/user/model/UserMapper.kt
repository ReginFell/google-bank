package com.regin.googlebank.core.ui.models.user.model

import com.regin.googlebank.core.data.models.UserResource

object UserMapper {

    fun map(userResource: UserResource) = User(userResource.userGuid, userResource.rating)
}