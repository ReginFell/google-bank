package com.regin.googlebank.core.data.models

data class UserResource(val userGuid: String, val rating: Int)