package com.regin.googlebank.core.ui.utils

import android.os.Build
import android.view.View
import androidx.annotation.ColorRes
import androidx.core.content.ContextCompat

object AmbientShadowCompat {

    fun setOutlineAmbientShadowColor(view: View, @ColorRes colorRes: Int) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
            view.outlineAmbientShadowColor = ContextCompat.getColor(view.context, colorRes)
            view.outlineSpotShadowColor = ContextCompat.getColor(view.context, colorRes)
        }
    }
}