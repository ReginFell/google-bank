package com.regin.googlebank.core.data.models

data class OfferResource(
        val guid: String,
        val offerIconUrl: String,
        val offerName: String,
        val offerMonthlyPayment: Float,
        val interestRateFrom: Float,
        val interestRateTo: Float? = null,
        val interestRateOld: Float? = null
)