package com.regin.googlebank.core.ui.models.loan

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class LoanFirstStepBuilder(
        val loanGuid: String,
        val loanTitle: String,
        val moneyAmount: Float,
        val duration: Int
) : Parcelable