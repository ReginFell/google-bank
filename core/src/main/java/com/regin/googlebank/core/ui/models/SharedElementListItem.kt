package com.regin.googlebank.core.ui.models

import android.view.View

class SharedElementListItem<T>(val item: T, val view: View, val transitionName: String)