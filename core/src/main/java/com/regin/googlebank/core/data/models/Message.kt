package com.regin.googlebank.core.data.models

sealed class Message(
    open val guid: String,
    open val userAvatar: String,
    open val isSender: Boolean
) {

    data class TextMessage(
        override val guid: String,
        override val userAvatar: String,
        override val isSender: Boolean,
        val text: String
    ) : Message(guid, userAvatar, isSender)

    data class CardChooserMessage(
        override val guid: String,
        override val userAvatar: String,
        override val isSender: Boolean,
        val cards: List<Card>

    ) : Message(guid, userAvatar, isSender)

}