package com.regin.googlebank.core.data.models

sealed class LoanDurationResource(open val duration: Int) {
    data class Small(override val duration: Int) : LoanDurationResource(duration)
    data class Regular(override val duration: Int) : LoanDurationResource(duration)
    data class Large(override val duration: Int) : LoanDurationResource(duration)
}